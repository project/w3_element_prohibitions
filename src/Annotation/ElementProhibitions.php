<?php

namespace Drupal\w3_element_prohibitions\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Element Prohibitions item annotation object.
 *
 * @see \Drupal\w3_element_prohibitions\Plugin\ElementProhibitionsManager
 * @see plugin_api
 *
 * @Annotation
 */
class ElementProhibitions extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
