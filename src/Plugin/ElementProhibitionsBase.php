<?php

namespace Drupal\w3_element_prohibitions\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Element Prohibitions plugins.
 */
abstract class ElementProhibitionsBase extends PluginBase implements ElementProhibitionsInterface {


  // Add common methods and abstract methods for your plugin type here.

}
