<?php

namespace Drupal\w3_element_prohibitions\Plugin\element_prohibitions;

use Drupal\w3_element_prohibitions\Plugin\ElementProhibitionsBase;

/**
 * @ElementProhibitions(
 *  id = "form",
 *  label = @Translation("Form"),
 * )
 */
class FormProhibition extends ElementProhibitionsBase {

  /**
   * {@inheritdoc}
   */
  public function resolve($hook, array $element): ?string {
    if ($hook !== 'form' || empty($element['form_build_id'])) {
      return NULL;
    }
    $functions = &drupal_static('w3_element_prohibitions', []);
    if (isset($functions['form']) && $functions['form'] > 1) {
      return 'child_form';
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getRegistryKey(array $element) {
    if (empty($element['form_build_id'])) {
      return NULL;
    }
    return 'form';
  }

}
