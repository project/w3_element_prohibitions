<?php

namespace Drupal\w3_element_prohibitions\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Element Prohibitions plugins.
 */
interface ElementProhibitionsInterface extends PluginInspectionInterface {

  /**
   * Resolve the W3 element prohibitions detected.
   *
   * @param mixed $hook
   *   The name of the theme hook.
   * @param array $element
   *   The render array element.
   *
   * @return string|null
   *   The alternative hook theme function. Otherwise NULL, indicating that
   *   the next resolver in the chain should be called.
   */
  public function resolve($hook, array $element);

  /**
   * Get the registry key if apply.
   *
   * @param array $element
   *   The render array element.
   *
   * @return string|null
   *   The alternative hook theme function. Otherwise NULL, indicating that
   *   the next resolver in the chain should be called.
   */
  public function getRegistryKey(array $element);

}
