<?php

namespace Drupal\w3_element_prohibitions\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Theme\Registry;

/**
 * Provides the Element Prohibitions plugin manager.
 */
class ElementProhibitionsManager extends DefaultPluginManager {

  /**
   * The theme registry used to render an output.
   *
   * @var \Drupal\Core\Theme\Registry
   */
  protected $themeRegistry;

  /**
   * Constructs a new ElementProhibitionsManagerManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/element_prohibitions', $namespaces, $module_handler, 'Drupal\w3_element_prohibitions\Plugin\ElementProhibitionsInterface', 'Drupal\w3_element_prohibitions\Annotation\ElementProhibitions');

    $this->alterInfo('w3_element_prohibitions_info');
    $this->setCacheBackend($cache_backend, 'w3_element_prohibitions_plugins');
  }

  /**
   * Sets the theme registry.
   *
   * @param \Drupal\Core\Theme\Registry $theme_registry
   *   The theme registry.
   *
   * @return $this
   */
  public function setThemeRegistry(Registry $theme_registry) {
    $this->themeRegistry = $theme_registry;
    return $this;
  }

  /**
   * Check for hook implementation.
   *
   * If an array of hook candidates is passed,
   * use the first one that has an implementation.
   *
   * @param mixed $hook
   *   The theme functions or array of theme functions.
   *
   * @return mixed|null
   *   The theme function found. Otherwise null.
   */
  public function findThemeHook($hook): ?string {
    $theme_registry = $this->themeRegistry->getRuntime();

    $theme_hooks = (array) $hook;
    $hook_found = NULL;
    // Check for hook implementation. If an array of hook candidates is passed,
    // use the first one that has an implementation.
    foreach ($theme_hooks as $candidate) {
      if ($theme_registry->has($candidate)) {
        $hook_found = $candidate;
        break;
      }
    }
    return $hook_found;
  }

  /**
   * {@inheritdoc}
   */
  public function resolve($hook, array $element): ?string {
    $hook_found = $this->findThemeHook($hook);
    $definitions = $this->getDefinitions();
    foreach ($definitions as $id => $def) {
      /** @var \Drupal\w3_element_prohibitions\Plugin\ElementProhibitionsInterface $instance */
      $instance = $this->createInstance($id, [
        'element' => $element,
        'hook' => $hook_found,
      ]);
      $result = $instance->resolve($hook_found, $element);
      if (!empty($result)) {
        return $result;
      }
    }
    return NULL;
  }

  /**
   * Register a deep key and increment the counter.
   *
   * @param array $element
   *   The render array element.
   *
   * @return string|null
   *   The registry key. Otherwise NULL, if no apply any registry key.
   */
  public function registerDeep(array $element): ?string {
    $key = $this->getRegistryKey($element);
    if ($key !== NULL) {
      $functions = &drupal_static('w3_element_prohibitions', []);
      if (empty($functions[$key])) {
        $functions[$key] = 1;
      }
      else {
        $functions[$key]++;
      }
    }
    return $key;
  }

  /**
   * Un-register a deep key.
   *
   * @param array $element
   *   The render array element.
   * @param string $key
   *   The key to un-register.
   */
  public function unregisterDeep(array $element, string $key = NULL): void {
    if ($key === NULL) {
      $key = $this->getRegistryKey($element);
    }
    if ($key !== NULL) {
      $functions = &drupal_static('w3_element_prohibitions', []);
      $functions[$key]--;
    }
  }

  /**
   * Get the registry that first apply.
   *
   * @param array $element
   *   The render array element.
   *
   * @return string|null
   *   The alternative hook theme function. Otherwise NULL, indicating that
   *   the next resolver in the chain should be called.
   */
  protected function getRegistryKey(array $element): ?string {
    $definitions = $this->getDefinitions();
    foreach ($definitions as $id => $def) {
      /** @var \Drupal\w3_element_prohibitions\Plugin\ElementProhibitionsInterface $instance */
      $instance = $this->createInstance($id, ['element' => $element]);
      if ($key = $instance->getRegistryKey($element)) {
        return $key;
      }
    }
    return NULL;
  }

}
