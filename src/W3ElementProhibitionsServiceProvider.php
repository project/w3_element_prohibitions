<?php

namespace Drupal\w3_element_prohibitions;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;
use Drupal\w3_element_prohibitions\Render\Renderer;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Registers services in the container.
 */
class W3ElementProhibitionsServiceProvider implements ServiceProviderInterface {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    $service_definition = $container->getDefinition('renderer');
    $service_definition->setClass(Renderer::class);
    $calls = [];
    $calls[] = [
      'setProhibitionsManager', [new Reference('plugin.manager.element_prohibitions')],
    ];
    $service_definition->setMethodCalls($calls);
    $container->setDefinition('renderer', $service_definition);
  }

}
